# IT Support FAQ Bot
This is retrieval based Chatbot based on FAQs found at Actionable Science. The data is
reformatted into a json with the form:
{section:
[[question, answer], [question, answer], ...]
}

Then I merged all this section into one big JSON file with all sections

Later I have transformed this JSON file to CSV and used the section names as class for the questions

Then I preprocess this csv file by stemming and tf-idf vectorizing the questions
The same process is applied to user's query.

I have used Support Vector Machine with linear kernel to classify the user's query into different classes
Once the class is found, I define a subset of questions belonging to this class and then use Cosine Similarity to find the most likely question
The answer associated with the question with maximum cosine similarity to user's query is served to the user.
# Use and Functionality
Starting up the bot will immediately allow you to ask questions and be given corresponding answers. If the bot does not provide the user the corrrect answer there is always a prompt to give other questions. This will then give the top 5 questions of the most similarity to the user input. Simply type in 'Q' to exit the bot. 

Various options are provided in case of mismatch.
These are Debug - Let's you know the class predicted and the question with maximum cosine similarity
          TOP5 - Gives answer to top 5 questions with cosine similarity to user's query in descending order
# Future Enhancements
Adding the ability to ask for more suggestions than just 5 when user input is misinterpreted by the bot

Providing the bot a vocabulary that can be modified to better parse user input

The ability of the bot to learn from user inputs and answers.
