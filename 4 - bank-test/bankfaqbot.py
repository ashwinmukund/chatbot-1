#importing relevant functions from libraries. The especially important functions include train_test_split as well as Lancaster Stemmer.
#Lancaster Stemmer is a stemming algorithm which means that variant words are reduced to their base form. An example would be the words
#connections, connective, connection would all be reduce to connect. 
import pandas as pd
import numpy as np
import pickle
import operator
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split as tts
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import LabelEncoder as LE
from sklearn.metrics.pairwise import cosine_similarity
import random
import nltk
nltk.download('punkt')

#The lancaster stemmer was originally selected for it computational speed 
#but was changed to the snowball stemmer due to the Lancaster stemmer having the habit of making smaller words becoming totally unclear. This change 
#immediately increased the algorithms accuracy
from nltk.stem.snowball import SnowballStemmer

stemmer = SnowballStemmer("english")

#this is a  helper function that cleans up user input for the algorithm to use. It simply tokenizes sentences and applies the stemmer to them
#for easier comprehension. This is also used for cleaning up the question dataset.
def cleanup(sentence):
    word_tok = nltk.word_tokenize(sentence)
    stemmed_words = [stemmer.stem(w) for w in word_tok]

    return ' '.join(stemmed_words)
#label encoder for the classes of questions in the dataset
le = LE()
#Converts the document to term frequency-inverser document frequencies, a statistic that represents how "important" a word is to a document.
#This essentially is the frequency of the word in the document. When first testing the TFidfvectorizer, we used min_df to remove irrelevant words
#but were unclear as to what number between 0 to 1 to use. We originally used 0.1, but after reading into documentation, it was switched to 1
#to increase accuracy. The 1 means if the algorithm doesnt find it in the document (the dataset), ignore it.
tfv = TfidfVectorizer(min_df=1, stop_words='english')

data = pd.read_csv('testdata.csv')
questions = data['Question'].values

X = []
for question in questions:
    X.append(cleanup(question))

#Our algorithm classifies question by classes so we will base our model on that concept as well
tfv.fit(X)
le.fit(data['Class'])

#actually transforming the data for prediction capabilities
X = tfv.transform(X)
y = le.transform(data['Class'])

#splits arrays of the dataset into random train and test subsets, after experimentation, we found that a test size of 1/5 of the dataset was optimal as opposed to 1/4 of the dataset which we changed while the random state parameter was simply a seed for random generation that did not matter.
#parameter didnt matter for accuracy
trainx, testx, trainy, testy = tts(X, y, test_size=.2, random_state=42)

model = SVC(kernel='linear')
model.fit(trainx, trainy)
print("SVC:", model.score(testx, testy))

#This is used for the feature of being able to request 5 additional questions to pick when the algorithm fails to give you the correct answer in ambiguous cases.
def get_max5(arr):
    ixarr = []
    for ix, el in enumerate(arr):
        ixarr.append((el, ix))
    ixarr.sort()

    ixs = []
    for i in ixarr[-5:]:
        ixs.append(i[1])

    return ixs[::-1]



def chat():
    cnt = 0
    print("PRESS Q to QUIT")
    #THE FOLLOWING WAS USED FOR DEBUG PURPOSES
    #print("TYPE \"DEBUG\" to Display Debugging statements.")
    #print("TYPE \"STOP\" to Stop Debugging statements.")
    #print("TYPE \"TOP5\" to Display 5 most relevent results")
    #print("TYPE \"CONF\" to Display the most confident result")
    #print()
    #print()
    #DEBUG = False
    #TOP5 = False

    print("Bot: Hi, Welcome to our service, how may I help you?")
    while True:
        usr = input("You: ")
        if  usr.lower() == 'hi':
            print("Bot: Hi!")
            continue
        if usr.lower() == 'yes':
            print("Bot: Yes!")
            continue

        if usr.lower() == 'no':
            print("Bot: No?")
            continue

        #if usr == 'DEBUG':
            #DEBUG = True
            #print("Debugging mode on")
            #continue

        #if usr == 'STOP':
            #DEBUG = False
            #print("Debugging mode off")
            #continue

        if usr == 'Q':
            print("Bot: It was good to be of help.")
            break

        #if usr == 'TOP5':
            #TOP5 = True
            #print("Will display 5 most relevent results now")
            #continue

        #if usr == 'CONF':
            #TOP5 = False
            #print("Only the most relevent result will be displayed")
            #continue

        t_usr = tfv.transform([cleanup(usr.strip().lower())])
        class_ = le.inverse_transform(model.predict(t_usr)[0])
        questionset = data[data['Class']==class_]

        #if DEBUG:
            #print("Question classified under category:", class_)
            #print("{} Questions belong to this class".format(len(questionset)))

        cos_sims = []
        for question in questionset['Question']:
            sims = cosine_similarity(tfv.transform([question]), t_usr)
            cos_sims.append(sims)
            
        ind = cos_sims.index(max(cos_sims))

        #if DEBUG:
            #question = questionset["Question"][questionset.index[ind]]
            #print("Assuming you asked: {}".format(question))

        if not TOP5:
            print("Bot:", data['Answer'][questionset.index[ind]])
            print(cos_sims[ind])
        #else:
            #inds = get_max5(cos_sims)
            #for ix in inds:
                #print("Question: "+data['Question'][questionset.index[ix]])
                #print("Answer: "+data['Answer'][questionset.index[ix]])
                #print(cos_sims[ix])
                #print('-'*50)

        print("\n"*2)
        outcome = input("Was this answer helpful? Yes/No: ").lower().strip()
        if outcome == 'yes':
            cnt = 0
        elif outcome == 'no':
            inds = get_max5(cos_sims)
            sugg_choice = input("Bot: Do you want me to suggest you questions ? Yes/No: ").lower()
            if sugg_choice == 'yes':
                q_cnt = 1
                for ix in inds:
                    print(q_cnt,"Question: "+data['Question'][questionset.index[ix]])
                    print(cos_sims[ix])
                    print('-'*50)
                    q_cnt += 1
                num = int(input("Please enter the question number you find most relevant: "))
                print("Bot: ", data['Answer'][questionset.index[inds[num-1]]])

chat()
